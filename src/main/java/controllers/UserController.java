package controllers;

import domain.User;
import repositories.entities.UserRepository;
import repositories.interfaces.IEntityRepository;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.util.LinkedList;

@Path("users")
public class UserController {

    @GET
    public String hello(){
        return "hello world!";
    }

    @GET
    @Path("/{id}")
    public Response getUserById(@PathParam("id") long id){
        String sql = "SELECT * FROM first WHERE id = "+id;
        IEntityRepository userrepo = new UserRepository();
        LinkedList<User> users = (LinkedList<User>) userrepo.query(sql);
        if(users.isEmpty()){
            return Response.status(Response.Status.NOT_FOUND)
                                    .entity("User does not exists!")
                                    .build();
        }else{
            return Response
                    .status(Response.Status.OK)
                    .entity(users.get(0))
                    .build();


        }
    }
}
