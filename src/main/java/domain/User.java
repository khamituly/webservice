package domain;

public class User {
    private long id;
    private String name;
    private String surname;

    public User(){

    }

    public User(String name, String surname){

        setName(name);
        setSurname(surname);
    }

    public User(long id, String name, String surname){
        setId(id);
        setName(name);
        setSurname(surname);
    }

    public long getId() {
        return id;
    }

    public void setId(long new_id) {
        this.id = new_id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public void setName(String x) {
        this.name = x;
    }

    public void setSurname(String x) {
        this.surname = x;
    }

    @Override
    public String toString() {
        return "id: "+id+", name : "+name+", surname: "+surname;
    }
}
