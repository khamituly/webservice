package repositories.entities;

import domain.User;
import repositories.db.PostgresRepository;
import repositories.interfaces.IDBRepository;
import repositories.interfaces.IEntityRepository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

public class UserRepository implements IEntityRepository<User> {
    private IDBRepository dbrepo;

    public UserRepository(){
        dbrepo = new PostgresRepository();
    }

    @Override
    public void add(User entity) {
        try { //prepare statement
            Statement stm = dbrepo.getConnection().createStatement();
            String sql = "Insert into first(id,name,surname)"+"values('"+entity.getId()+"','"+entity.getName()+"','"+entity.getSurname()+"')";
            stm.execute(sql);
        } catch (SQLException ex){
           ex.printStackTrace();
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    public void update(User entity) {

    }

    @Override
    public void remove(User entity) {

    }

    @Override
    public Iterable<User> query(String sql) {
        try {
            Statement stm = dbrepo.getConnection().createStatement();
            ResultSet rs =stm.executeQuery(sql);
            LinkedList<User> users = new LinkedList<>();
            while(rs.next()){
                User user = new User(
                        rs.getLong("id"),
                        rs.getString("name"),
                        rs.getString("surname")
                );
                users.add(user);
            }
            return users;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
